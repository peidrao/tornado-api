from tornado.web import Application

from db.db import db
from handlers.pets import PetsListHandler
from handlers.user import UserDetailsHandler, UserListHandler


def make_routes():
    endpoints = [
        ("/users", UserListHandler),
        ("/users/(.*)", UserDetailsHandler),
        ("/pets/", PetsListHandler),
    ]
    return Application(endpoints, debug=True, db=db)
