import json
from tornado import testing
from router import make_routes


class UsersListTestCase(testing.AsyncHTTPTestCase):

    def get_app(self):
        return make_routes()

    def test_list_users(self):
        response = self.fetch('/users', method="GET")
        self.assertEqual(response.code, 200)
        data = json.loads(response.body.decode('utf-8'))
        self.assertTrue(data['users'])
