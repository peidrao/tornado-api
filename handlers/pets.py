from bson import ObjectId
from tornado.web import RequestHandler
from tornado.escape import json_decode

from db.db import pets_db


class PetsListHandler(RequestHandler):
    async def post(self):
        data = json_decode(self.request.body)
        data['_id'] = str(ObjectId())
        new_pet = await pets_db.insert_one(data)
        created_pet = await pets_db.find_one({
            "_id": new_pet.inserted_id
        })

        self.set_status(201)

        return self.write(created_pet)

    async def get(self):
        pets = await pets_db.find().to_list(100)
        return self.write({'pets': pets})
