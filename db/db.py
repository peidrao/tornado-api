from motor import motor_tornado
from dotenv import dotenv_values

config = dotenv_values(".env")


URL_CONNECT = config.get('URL_CONNECT')

client = motor_tornado.MotorClient(URL_CONNECT)
db = client.collections

users_db = db.users
pets_db = db.pets
