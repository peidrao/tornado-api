from bson import ObjectId
from tornado.web import RequestHandler
from tornado.escape import json_decode

from db.db import users_db


class UserListHandler(RequestHandler):
    async def post(self):
        data = json_decode(self.request.body)
        data['_id'] = str(ObjectId())
        new_user = await users_db.insert_one(data)
        created_user = await users_db.find_one({
            "_id": new_user.inserted_id
        })

        self.set_status(201)

        return self.write(created_user)

    async def get(self):
        users = await users_db.find().to_list(100)
        return self.write({'users': users})


class UserDetailsHandler(RequestHandler):
    async def get(self, user_id):
        if user_id:
            if (
                user := await users_db.find_one({"_id": user_id})
                ) is not None:
                return self.write(user)
            self.set_status(404)
            return self.write({'message': 'user not found'})

    async def delete(self, user_id):
        user = await users_db.delete_one({'_id': user_id})
        if user.deleted_count == 1:
            self.set_status(204)
            return self.finish()

        self.set_status(404)
        return self.write({'message': 'An error occurred while deleting the user'})

    async def put(self, user_id):
        user = json_decode(self.request.body)
        await users_db.update_one(
            {"_id": user_id}, {"$set": user}
        )

        if (
            update_user := await users_db.find_one(
                {'_id': user_id})
            ) is not None:
            return self.write(update_user)

        self.set_status(400)
        self.write({'message': 'An error occurred while updating the user'})
