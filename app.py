from tornado.ioloop import IOLoop

from router import make_routes


if __name__ == "__main__":
    app = make_routes()
    app.listen(3000)
    IOLoop.current().start()
